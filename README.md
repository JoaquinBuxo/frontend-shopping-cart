# Frontend Shopping Cart

You can see here a preview of the app: https://frontend-shopping-cart.vercel.app/

## Get Start

### `npm install`

You will install all the dependicies to make the app work

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.
