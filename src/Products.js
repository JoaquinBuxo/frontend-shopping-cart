export const Products = [
  {
    imgProduct: "img/tshirt.jpg",
    nameProduct: "Shirt",
    codeProduct: "X7R2OPX",
    priceProduct: 20,
    currencyProduct: "€",
    code: "TSHIRT",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sodales semper elit sit amet interdum. Praesent volutpat sed elit vel consectetur. Nulla tempus tincidunt ex, sit amet semper ipsum imperdiet varius. In rutrum aliquam nisl, sagittis faucibus felis bibendum id.",
  },
  {
    imgProduct: "img/mug.jpg",
    nameProduct: "Mug",
    codeProduct: "X2G2OPZ",
    priceProduct: 5,
    currencyProduct: "€",
    code: "MUG",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sodales semper elit sit amet interdum. Praesent volutpat sed elit vel consectetur. Nulla tempus tincidunt ex, sit amet semper ipsum imperdiet varius. In rutrum aliquam nisl, sagittis faucibus felis bibendum id.",
  },
  {
    imgProduct: "img/cap.jpg",
    nameProduct: "Cap",
    codeProduct: "X3W2OPY",
    priceProduct: 10,
    currencyProduct: "€",
    code: "CAP",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sodales semper elit sit amet interdum. Praesent volutpat sed elit vel consectetur. Nulla tempus tincidunt ex, sit amet semper ipsum imperdiet varius. In rutrum aliquam nisl, sagittis faucibus felis bibendum id.",
  },
];
