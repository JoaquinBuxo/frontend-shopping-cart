const ProductInfo = (props) => {
  return (
    <div className="col-product" onClick={props.onClick}>
      <figure className="product-image">
        <img src={props.imgProduct} alt={props.nameProduct} />
        <div className="product-description">
          <h1>{props.nameProduct}</h1>
          <p className="product-code">Product code {props.codeProduct}</p>
        </div>
      </figure>
    </div>
  );
};

export default ProductInfo;
