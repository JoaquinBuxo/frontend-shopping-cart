const ProductTotal = (props) => {
  return (
    <div className="col-total">
      <span className="product-price">{props.priceTotal}</span>
      <span className="product-currency currency">{props.currencyProduct}</span>
    </div>
  );
};

export default ProductTotal;
