const SummaryItem = (props) => {
  return (
    <li>
      <span className={`summary-${props.itemNumberClass}`}>
        {props.itemNumberText}
      </span>
      <span className={`summary-${props.itemPriceClass}`}>
        {props.itemPriceText}
        <span className="currency">{props.itemPriceCurrency}</span>
      </span>
    </li>
  );
};

export default SummaryItem;
