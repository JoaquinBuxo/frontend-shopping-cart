const ProductItem = (props) => {
  return <li className="product row">{props.children}</li>;
};

export default ProductItem;
