const Title = (props) => {
  return <h1 className="main">{props.titleText}</h1>;
};

export default Title;
