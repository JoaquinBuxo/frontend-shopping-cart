import Modal from "react-modal";

Modal.setAppElement("#root");

const ProductModal = (props) => {
  return (
    <Modal
      isOpen={props.isOpenModal}
      onRequestClose={props.createModal}
      className="modal"
    >
      <section className="img-product-modal">
        <img
          className="img-modal"
          src={props.dataProductModal.imgProduct}
          alt={props.dataProductModal.nameProduct}
        />
      </section>
      <aside className="info-product-modal">
        <button className="button-close-modal" onClick={props.createModal}>
          <img
            className="img-modal"
            src="img/cross-icon.png"
            alt="Close Modal"
          />
        </button>
        <h3 className="name-product-modal">
          {props.dataProductModal.nameProduct}
          <span>
            {props.dataProductModal.priceProduct}
            {props.dataProductModal.currencyProduct}
          </span>
        </h3>
        <div className="description-product-modal">
          {props.dataProductModal.description}
        </div>
        <div className="code-product-modal">
          Product code {props.dataProductModal.codeProduct}
        </div>
        <button
          className="button-purple"
          onClick={() => {
            props.handleQuantityAdd(props.dataProductModal.code);
            props.createModal(props.dataProductModal);
          }}
        >
          Add to cart
        </button>
      </aside>
    </Modal>
  );
};

export default ProductModal;
