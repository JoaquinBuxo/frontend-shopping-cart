const ProductQuantity = (props) => {
  return (
    <div className="col-quantity">
      <button
        className="count"
        onClick={props.handleQuantityRemove}
        disabled={props.quantity == 0}
      >
        -
      </button>

      <input
        type="text"
        className="product-quantity"
        value={props.quantity}
        readOnly
      />
      <button className="count" onClick={props.handleQuantityAdd}>
        +
      </button>
    </div>
  );
};

export default ProductQuantity;
