import Title from "./Title";
import ProductsListHeader from "./ProductsListHeader";
import ProductItem from "./ProductItem";
import ProductInfo from "./ProductInfo";
import ProductPrice from "./ProductPrice";
import ProductQuantity from "./ProductQuantity";
import ProductTotal from "./ProductTotal";
import ProductModal from "./ProductModal";

const ProductsContainer = (props) => {
  return (
    <section className="products">
      {/* Creating it as a component allows me to reuse the code for future chunks where it is reused with other properties */}
      <Title titleText="Shopping cart" />
      {/* This component is just to make the code easier to read */}
      <ProductsListHeader />
      <ul className="products-list">
        {props.products.map((data, key) => (
          // With this division of components it allows me to use each one separately, and when creating the loop the code is more
          // flexible and more objects can be added dynamically
          <ProductItem key={key}>
            <ProductInfo
              imgProduct={data.imgProduct}
              nameProduct={data.nameProduct}
              codeProduct={data.codeProduct}
              onClick={() => props.createModal(data)}
            />
            <ProductQuantity
              handleQuantityRemove={() => props.handleQuantityRemove(data.code)}
              handleQuantityAdd={() => props.handleQuantityAdd(data.code)}
              quantity={props.checkout.getNumProduct(data.code)}
            />
            <ProductPrice
              priceProduct={data.priceProduct}
              currencyProduct={data.currencyProduct}
            />
            <ProductTotal
              priceTotal={props.checkout.totalPricePerProduct(
                data.code,
                data.priceProduct
              )}
              currencyProduct={data.currencyProduct}
            />
          </ProductItem>
        ))}
      </ul>
      {/* Creation of the modal, it only appears when you click on the info (photo and product name) */}
      <ProductModal
        isOpenModal={props.isOpenModal}
        createModal={props.createModal}
        dataProductModal={props.dataProductModal}
        handleQuantityAdd={() =>
          props.handleQuantityAdd(props.dataProductModal.code)
        }
      />
    </section>
  );
};

export default ProductsContainer;
