import SummaryItem from "./SummaryItem";
import Title from "./Title";

const SummaryContainer = (props) => {
  return (
    <aside className="summary">
      {/* Creating it as a component allows me to reuse the code for future chunks where it is reused with other properties */}
      <Title titleText="Order Summary" />
      <ul className="summary-items wrapper border">
        {/* Creating it as a component allows me to reuse the code for future chunks where it is reused with other properties */}
        <SummaryItem
          itemNumberClass="items-number"
          itemNumberText={`${props.checkout.numProducts.length} Items`}
          itemPriceClass="items-price"
          itemPriceText={props.checkout.totalPriceWithoutDiscount()}
          itemPriceCurrency="€"
        />
      </ul>
      <div className="summary-discounts wrapper-half border">
        <h2>Discounts</h2>
        <ul>
          {/* and here we are already reusing the component */}
          <SummaryItem
            itemNumberClass="items-number"
            itemNumberText="2x1 Mug offer"
            itemPriceClass="items-price"
            itemPriceText={-props.checkout.promotion2x1Product("MUG")}
            itemPriceCurrency="€"
          />
          <SummaryItem
            itemNumberClass="items-number"
            itemNumberText="x3 Shirt offer"
            itemPriceClass="items-price"
            itemPriceText={-props.checkout.discountPerCentProduct("TSHIRT")}
            itemPriceCurrency="€"
          />
          <SummaryItem
            itemNumberClass="items-number"
            itemNumberText="Promo code"
            itemPriceClass="items-price"
            itemPriceText="0"
            itemPriceCurrency="€"
          />
        </ul>
      </div>
      <div className="summary-total wrapper">
        <ul>
          <SummaryItem
            itemNumberClass="total-cost"
            itemNumberText="Total cost"
            itemPriceClass="total-price"
            itemPriceText={props.checkout.total()}
            itemPriceCurrency="€"
          />
        </ul>
        <button className="button-purple" type="submit">
          Checkout
        </button>
      </div>
    </aside>
  );
};

export default SummaryContainer;
