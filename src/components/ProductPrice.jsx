const ProductPrice = (props) => {
  return (
    <div className="col-price">
      <span className="product-price">{props.priceProduct}</span>
      <span className="product-currency currency">{props.currencyProduct}</span>
    </div>
  );
};

export default ProductPrice;
