import "./App.css";
import Checkout from "./Checkout";
import ProductsContainer from "./components/ProductsContainer";
import SummaryContainer from "./components/SummaryContainer";
import { useState } from "react";
import { Products } from "./Products";

function App() {
  const checkout = new Checkout(Products);
  const [quantity, setQuantity] = useState([]);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [dataProductModal, setDataProductModal] = useState([]);

  /**
   * Handles each time an object is added
   */
  const handleQuantityAdd = (code) => {
    checkout.scan(code);
    setQuantity([...checkout.numProducts]);
    checkout.totalPriceWithoutDiscount();
  };

  /**
   * Handles each time an object is removed
   */
  const handleQuantityRemove = (code) => {
    checkout.removeProduct(code);
    setQuantity([...checkout.numProducts]);
    checkout.totalPriceWithoutDiscount();
  };

  /**
   * Create the modal with the data of the product to be detailed
   */
  const createModal = (dataProduct) => {
    setIsOpenModal(!isOpenModal);
    setDataProductModal(dataProduct);
  };

  return (
    <main className="App">
      {/* divided between the two bigger components to make it more organized and easier to read and understand*/}
      <ProductsContainer
        handleQuantityRemove={handleQuantityRemove}
        handleQuantityAdd={handleQuantityAdd}
        checkout={checkout}
        products={Products}
        createModal={createModal}
        isOpenModal={isOpenModal}
        dataProductModal={dataProductModal}
      />
      <SummaryContainer checkout={checkout} />
    </main>
  );
}

export default App;
