export default class Checkout {
  constructor(Products) {
    this.numProducts = Checkout.numProducts;
    this.Products = Products;
    this.PRICE_TSHIRT = this.Products[0].priceProduct;
    this.PRICE_MUG = this.Products[1].priceProduct;
    this.PRICE_CAP = this.Products[2].priceProduct;
  }

  /**
   * Scans a product adding it to the current cart.
   * @param code The product identifier
   * @returns itself to allow function chaining
   */
  scan(code) {
    this.numProducts.push(code);
    return this;
  }

  /**
   * Remove a product from the current cart
   */
  removeProduct(code) {
    let indexToRemove = this.numProducts.lastIndexOf(code);
    this.numProducts.splice(indexToRemove, 1);
    return this.numProducts;
  }

  /**
   * Keep the number of products in the array to prevent reset
   */
  static get numProducts() {
    Checkout._numProducts = this._numProducts || [];
    return Checkout._numProducts;
  }

  /**
   * Return number of products with the same code
   */
  getNumProduct(code) {
    return this.numProducts.filter((product) => product === code).length;
  }

  /**
   * Return the total price per one product
   */
  totalPricePerProduct = (code, price) => {
    return this.getNumProduct(code) * price;
  };

  /**
   * Return total price without the discounts
   */
  totalPriceWithoutDiscount() {
    let totalPriceWithoutDiscount = 0;

    this.Products.map(
      (element) =>
        (totalPriceWithoutDiscount += this.totalPricePerProduct(
          element.code,
          element.priceProduct
        ))
    );

    return totalPriceWithoutDiscount;
  }

  /**
   * Return the discount as a percentage
   */
  discountPerCent(price, discount) {
    return (price * discount) / 100;
  }

  /**
   * Returns the discount by percentage of the product
   */
  discountPerCentProduct(code) {
    let productsCode = this.getNumProduct(code);
    let discount = 0;

    if (productsCode > 2) {
      discount = this.discountPerCent(productsCode * this.PRICE_TSHIRT, 5);
    } else {
      discount = 0;
    }
    return discount;
  }

  /**
   * Apply the 2x1 promotion
   */
  promotion2x1Product(code) {
    let discount = 0;
    this.totalPricePerProduct(code, this.PRICE_MUG) % 2 == 0
      ? (discount = this.totalPricePerProduct(code, this.PRICE_MUG) / 2)
      : (discount =
          this.totalPricePerProduct(code, this.PRICE_MUG) / 2 -
          this.PRICE_MUG / 2);

    return discount;
  }

  /**
   * Returns the value of all cart products with the discounts applied.
   */
  total() {
    let totalPriceWithoutDiscount = this.totalPriceWithoutDiscount();
    let tshirtsDiscount = this.discountPerCentProduct("TSHIRT");
    let mugsPromotion = this.promotion2x1Product("MUG");

    const totalPrice =
      totalPriceWithoutDiscount - tshirtsDiscount - mugsPromotion;

    return totalPrice;
  }
}
